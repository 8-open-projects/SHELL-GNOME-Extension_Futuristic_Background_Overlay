# GNOME-SHELL-Extension Futuristic-Background-Overlay

![IMG](GNOME-SHELL-Extension_Futuristic-Background-Overlay.png)

---
---

# changes you must did when you will use all functions
## weather
* set the "COUNTRY_KEY" for your region witch you will get from accuweather location-API
* set the "APPID". It's the key from your app on accuweather

## Used Fonts
* https://fonts.google.com/specimen/Lato
* http://www.dafont.com/arenq.font
* http://www.emmeranrichard.fr/portfolio/anurati-font/

## Reference how to use label on Background
* https://extensions.gnome.org/extension/889/background-logo/

## Background Image
* https://wallpapersontheweb.net/wallpapers/l/moon_dark-19150.jpg

## Weather-API
* https://developer.accuweather.com

## Weather Icon
* http://www.freepik.com/free-vector/weather-icons_1196720.htm
