const Clutter = imports.gi.Clutter;
const Gio = imports.gi.Gio;
const Lang = imports.lang;
const St = imports.gi.St;
const Background = imports.ui.background;
const Me = imports.misc.extensionUtils.getCurrentExtension();

const Layout = imports.ui.layout;
const Main = imports.ui.main;
const Tweener = imports.ui.tweener;
const Mainloop = imports.mainloop;
const Soup = imports.gi.Soup;

/*########################################################################*/
let _httpSession; // for networking

// weather URL-GET Informations
let COUNTRY_KEY = null; // the country code (you will get this by accuweather location-API)
let LANGUAGE = "de-de"; // your language type
let DETAILS = "false"; // when you would like to get more weather informations
let METRIC = "true"; // ...
let APPID = null // your accuweather key
let WEATHER_URL = "http://dataservice.accuweather.com/forecasts/v1/hourly/1hour/" + COUNTRY_KEY + "?apikey=" + APPID + "&language=" + LANGUAGE + "&details=" + DETAILS + "&metric=" + METRIC;

/*########################################################################*/
let timeout; // for the loop
let button; // panel button to start and stop the overlay

let isShown = false; // to switch between show and not show the overlay

let monitor = Main.layoutManager.primaryMonitor; // get the monitor the get the sizes

let weekday = new Array(7);
weekday[0] = "sunday";
weekday[1] = "monday";
weekday[2] = "tuesday";
weekday[3] = "wednesday";
weekday[4] = "thursday";
weekday[5] = "friday";
weekday[6] = "saturday";

let monthNames = new Array(12);
monthNames[0] = "jan";
monthNames[1] = "feb";
monthNames[2] = "mar";
monthNames[3] = "apr";
monthNames[4] = "may";
monthNames[5] = "jun";
monthNames[6] = "jul";
monthNames[7] = "aug";
monthNames[8] = "sep";
monthNames[9] = "okt";
monthNames[10] = "nov";
monthNames[11] = "dec";

let textLogo = "C3nPr0Un1";

/*########################################################################*/

// the background overlay Class witch contains the complitly logic
// to overlay everything on the background
const BackgroundLogo = new Lang.Class({
  Name: 'BackgroundLogo',

  _init: function(bgManager) {

    // labels
    let logo;

    let lableDateHour;
    let lableDateMinute;
    let lableDateTextFirst;
    let lableDateTextSecond;

    // a array witch contains created labels
    // to add or remove them easy from actor
    this.labelsDateArray = [];

    let lableWeather;
    this.labelsWeatherArray = [];

    // its a boolean to start some tinks only once
    // because _onUpdate will start in a loop
    this.startSystem = true;

    let time_date = new Date();
    let time_weather = new Date();

    /*########################################*/

    this._bgManager = bgManager;

    this.actor = new St.Widget({
      layout_manager: new Clutter.BinLayout(),
      opacity: 255
    });
    bgManager._container.add_actor(this.actor);

    let monitorIndex = bgManager._monitorIndex;
    let constraint = new Layout.MonitorConstraint({
      index: monitorIndex
    });
    this.actor.add_constraint(constraint);

    /*########################################*/

    // set shown to true that on at second panel button click the overlay
    // will close
    isShown = true;

    // start the overlaying in a loop
    test();
  },
  /*######################################################*/
  _onUpdate: function() {
    let time_current = new Date();
    if (this.startSystem || this.time_date.getMinutes() !== time_current.getMinutes())
      this._addTimeBottomRight();

    if (APPID != null && (this.startSystem || this.time_weather.getHours() !== time_current.getHours()))
      _getRequestWeather(WEATHER_URL);

    if (this.startSystem) {
      this._addLogo();
      this.startSystem = false;
    }
  },
  /*######################################################*/
  _addTimeBottomRight: function() {
    // remove lables when they are display
    if (this.labelsDateArray.length > 0) {
      for (let element of this.labelsDateArray) {
        this.actor.remove_actor(element);
        element = null;
      }
      this.labelsDateArray = [];
    }

    // create some lable for time to the display
    this.time_date = new Date();
    let moveM = 33;

    this.lableDateHour = new St.Label({
      style_class: 'lable-date date-hour',
      text: "" + (this.time_date.getHours() < 10 ? '0' : '') + this.time_date.getHours()
    });
    this.lableDateHour.set_position(monitor.x + Math.floor(monitor.width - (this.lableDateHour.width * moveM)),
      monitor.y + Math.floor(monitor.height - this.lableDateHour.height - 280));
    this.labelsDateArray.push(this.lableDateHour);

    this.lableDateMinute = new St.Label({
      style_class: 'lable-date date-minutes',
      text: "" + (this.time_date.getMinutes() < 10 ? '0' : '') + this.time_date.getMinutes()
    });
    this.lableDateMinute.set_position(monitor.x + Math.floor(monitor.width - (this.lableDateHour.width * (moveM - moveM / 2.4))),
      monitor.y + Math.floor(monitor.height - this.lableDateMinute.height - 280));
    this.labelsDateArray.push(this.lableDateMinute);

    let dayTextFirst = weekday[this.time_date.getDay()] + "  |  " + monthNames[this.time_date.getMonth()] + " ";
    dayTextFirst = dayTextFirst.toUpperCase();
    this.lableDateTextFirst = new St.Label({
      style_class: 'lable-date date-day-text-first',
      text: dayTextFirst,
    });
    this.lableDateTextFirst.set_position(monitor.x + Math.floor(monitor.width - (this.lableDateHour.width * (moveM - 1.2))),
      monitor.y + Math.floor(monitor.height - this.lableDateTextFirst.height - 72));
    this.labelsDateArray.push(this.lableDateTextFirst);

    this.lableDateTextSecond = new St.Label({
      style_class: 'lable-date date-day-text-second',
      text: "" + (this.time_date.getUTCDate() < 10 ? '0' : '') + this.time_date.getUTCDate() + " " + this.time_date.getFullYear()
    });
    this.lableDateTextSecond.set_position(monitor.x + Math.floor(monitor.width - (this.lableDateHour.width * (moveM - 1.2)) + (this.lableDateTextFirst.width * 2.1)),
      monitor.y + Math.floor(monitor.height - this.lableDateTextSecond.height - 80));
    this.labelsDateArray.push(this.lableDateTextSecond);

    // add some lable for time to the display
    for (let element of this.labelsDateArray)
      this.actor.add_actor(element);
  },
  /*######################################################*/
  _addLogo: function() {
    if (this.logo != null) {
      this.actor.remove_actor(this.logo);
      this.logo = null;
    }
    this.logo = new St.Label({
      style_class: 'logo',
      text: textLogo
    });
    this.logo.set_position(monitor.x + Math.floor(monitor.width / 2 - this.logo.width * 4.5), monitor.y + 40);
    this.actor.add_actor(this.logo);
  },
  /*######################################################*/
  _backgroundDestroyed: function() {
    isShown = false;

    if (this.labelsDateArray.length > 0) {
      for (let element of this.labelsDateArray) {
        this.actor.remove_actor(element);
        element = null;
      }
      this.labelsDateArray = [];
    }

    if (this.labelsWeatherArray.length > 0) {
      for (let element of this.labelsWeatherArray) {
        this.actor.remove_actor(element);
        element = null;
      }
      this.labelsWeatherArray = [];
    }

    this.actor.remove_actor(this.logo);

    Mainloop.source_remove(timeout);

    if (this._bgManager._backgroundSource) // background swapped
      this._bgManager.backgroundActor.connect('destroy',
        Lang.bind(this, this._backgroundDestroyed));
    else // bgManager destroyed
      this.actor.destroy();
  },
  /*######################################################*/
  _addWeather: function(json, status) {
    this.time_weather = new Date();
    if (this.labelsWeatherArray.length > 0) {
      for (let element of this.labelsWeatherArray) {
        this.actor.remove_actor(element);
        element = null;
      }
      this.labelsWeatherArray = [];
    }

    let resultText = "";

    if (json == null) {
      resultText = status;
    } else {
      let temp = json[0].Temperature.Value + "°" + json[0].Temperature.Unit;
      resultText = temp;

      let gicon = null;

      let iconNum = json[0].WeatherIcon;
      if (iconNum > 0 && iconNum <= 6)
        gicon = Gio.icon_new_for_string(Me.path + "/icons/icon_sun.svg");
      else if (iconNum > 6 && iconNum <= 11)
        gicon = Gio.icon_new_for_string(Me.path + "/icons/icon_cloud.svg");
      else if (iconNum > 11 && iconNum <= 18)
        gicon = Gio.icon_new_for_string(Me.path + "/icons/icon_rain.svg");
      else
        gicon = Gio.icon_new_for_string(Me.path + "/icons/icon_night.svg");

      if (gicon != null) {
        this._icon = new St.Icon({
          style_class: 'weatherIcon',
          gicon: gicon
        });
        this._icon.set_position(monitor.x + 30, monitor.y + 40);
        this.labelsWeatherArray.push(this._icon);
      }
    }

    this.lableWeather = new St.Label({
      style_class: 'weather_temp',
      text: resultText
    });
    this.lableWeather.set_position(monitor.x + 110, monitor.y + 80);
    this.labelsWeatherArray.push(this.lableWeather);


    for (let element of this.labelsWeatherArray)
      this.actor.add_actor(element);
  }
});

/*######################################################*/
// get send weather request and get json weather response
// than call addWeater too add weather label to background
function _getRequestWeather(url) {
  // Create your message
  _httpSession = new Soup.Session();
  let message = Soup.Message.new('GET', url);

  // Send the message and retrieve the data
  _httpSession.queue_message(message, Lang.bind(this, function(_httpSession, message) {
    if (message.status_code !== 200) {
      forEachBackgroundManager(function(bgManager) {
        bgManager._logo._addWeather(null, message.status_code);
      });
      return;
    };
    let json = JSON.parse(message.response_body.data);
    // call the background class to display the weather datas on background
    forEachBackgroundManager(function(bgManager) {
      bgManager._logo._addWeather(json, 200);
    });
  }));
}

/*#######################################################################*/

// start the overlay in a loop the update the time
// and every other that can chaning
function test() {
  timeout = Mainloop.timeout_add(1000, function() {
    forEachBackgroundManager(function(bgManager) {
      bgManager._logo._onUpdate();
    });
    return true;
  });
}

/*#######################################################################*/

function forEachBackgroundManager(func) {
  Main.overview._bgManagers.forEach(func);
  Main.layoutManager._bgManagers.forEach(func);
}

let monitorsChangedId = 0;
let startupPreparedId = 0;

// function wen click on created panel button
function _onButtonClick() {
  // when the overlay is always shown than clean everything
  if (isShown) {
    disable();
  } else { // when not shown than start overlay for all backgrounds
    let startBackgroundOverlay = function() {
      forEachBackgroundManager(function(bgManager) {
        bgManager._logo = new BackgroundLogo(bgManager);
      });
    };

    monitorsChangedId = Main.layoutManager.connect('monitors-changed', startBackgroundOverlay);
    startupPreparedId = Main.layoutManager.connect('startup-prepared', startBackgroundOverlay);
    startBackgroundOverlay();
  }
}

/*#########################################################*/

// init on adding this extension a button with information to
// start and stop this extension
function init() {
  button = new St.Bin({
    style_class: 'panel-button',
    reactive: true,
    can_focus: true,
    x_fill: true,
    y_fill: false,
    track_hover: true
  });
  let icon = new St.Icon({
    icon_name: 'system-run-symbolic',
    style_class: 'system-status-icon'
  });

  button.set_child(icon);
  button.connect('button-press-event', _onButtonClick);

  // session initialisatiron
  _httpSession = new Soup.SessionAsync({
    ssl_use_system_ca_file: true
  });
  Soup.Session.prototype.add_feature.call(_httpSession, new Soup.ProxyResolverDefault());
}

// when enable the extension show a button in the top panel
function enable() {
  Main.panel._rightBox.insert_child_at_index(button, 0);
}

// when disable this extenson than close all opend operations
function disable() {
  if (monitorsChangedId)
    Main.layoutManager.disconnect(monitorsChangedId);
  monitorsChangedId = 0;

  if (startupPreparedId)
    Main.layoutManager.disconnect(startupPreparedId);
  startupPreparedId = 0;

  // start destroying from own generated background thinks and delete the created
  forEachBackgroundManager(function(bgManager) {
    bgManager._logo._backgroundDestroyed();
    delete bgManager._logo;
  });
}
